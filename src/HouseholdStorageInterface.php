<?php

namespace Drupal\household;

use Drupal\Core\Entity\ContentEntityStorageInterface;
use Drupal\Core\Session\AccountInterface;
use Drupal\Core\Language\LanguageInterface;
use Drupal\household\Entity\HouseholdInterface;

/**
 * Defines the storage handler class for Household entities.
 *
 * This extends the base storage class, adding required special handling for
 * Household entities.
 *
 * @ingroup household
 */
interface HouseholdStorageInterface extends ContentEntityStorageInterface {

  /**
   * Gets a list of Household revision IDs for a specific Household.
   *
   * @param \Drupal\household\Entity\HouseholdInterface $entity
   *   The Household entity.
   *
   * @return int[]
   *   Household revision IDs (in ascending order).
   */
  public function revisionIds(HouseholdInterface $entity);

  /**
   * Gets a list of revision IDs having a given user as Household author.
   *
   * @param \Drupal\Core\Session\AccountInterface $account
   *   The user entity.
   *
   * @return int[]
   *   Household revision IDs (in ascending order).
   */
  public function userRevisionIds(AccountInterface $account);

  /**
   * Counts the number of revisions in the default language.
   *
   * @param \Drupal\household\Entity\HouseholdInterface $entity
   *   The Household entity.
   *
   * @return int
   *   The number of revisions in the default language.
   */
  public function countDefaultLanguageRevisions(HouseholdInterface $entity);

  /**
   * Unsets the language for all Household with the given language.
   *
   * @param \Drupal\Core\Language\LanguageInterface $language
   *   The language object.
   */
  public function clearRevisionsLanguage(LanguageInterface $language);

}
