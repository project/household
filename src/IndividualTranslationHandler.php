<?php

namespace Drupal\household;

use Drupal\content_translation\ContentTranslationHandler;

/**
 * Defines the translation handler for individual.
 */
class IndividualTranslationHandler extends ContentTranslationHandler {

  // Override here the needed methods from ContentTranslationHandler.

}
