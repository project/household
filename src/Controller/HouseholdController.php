<?php

namespace Drupal\household\Controller;

use Drupal\Component\Utility\Xss;
use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\DependencyInjection\ContainerInjectionInterface;
use Drupal\Core\Url;
use Drupal\household\Entity\HouseholdInterface;

/**
 * Class HouseholdController.
 *
 *  Returns responses for Household routes.
 */
class HouseholdController extends ControllerBase implements ContainerInjectionInterface {

  /**
   * Displays a Household  revision.
   *
   * @param int $household_revision
   *   The Household  revision ID.
   *
   * @return array
   *   An array suitable for drupal_render().
   */
  public function revisionShow($household_revision) {
    $household = $this->entityManager()->getStorage('household')->loadRevision($household_revision);
    $view_builder = $this->entityManager()->getViewBuilder('household');

    return $view_builder->view($household);
  }

  /**
   * Page title callback for a Household  revision.
   *
   * @param int $household_revision
   *   The Household  revision ID.
   *
   * @return string
   *   The page title.
   */
  public function revisionPageTitle($household_revision) {
    $household = $this->entityManager()->getStorage('household')->loadRevision($household_revision);
    return $this->t('Revision of %title from %date', ['%title' => $household->label(), '%date' => format_date($household->getRevisionCreationTime())]);
  }

  /**
   * Generates an overview table of older revisions of a Household .
   *
   * @param \Drupal\household\Entity\HouseholdInterface $household
   *   A Household  object.
   *
   * @return array
   *   An array as expected by drupal_render().
   */
  public function revisionOverview(HouseholdInterface $household) {
    $account = $this->currentUser();
    $langcode = $household->language()->getId();
    $langname = $household->language()->getName();
    $languages = $household->getTranslationLanguages();
    $has_translations = (count($languages) > 1);
    $household_storage = $this->entityManager()->getStorage('household');

    $build['#title'] = $has_translations ? $this->t('@langname revisions for %title', ['@langname' => $langname, '%title' => $household->label()]) : $this->t('Revisions for %title', ['%title' => $household->label()]);
    $header = [$this->t('Revision'), $this->t('Operations')];

    $revert_permission = (($account->hasPermission("revert all household revisions") || $account->hasPermission('administer household entities')));
    $delete_permission = (($account->hasPermission("delete all household revisions") || $account->hasPermission('administer household entities')));

    $rows = [];

    $vids = $household_storage->revisionIds($household);

    $latest_revision = TRUE;

    foreach (array_reverse($vids) as $vid) {
      /** @var \Drupal\household\HouseholdInterface $revision */
      $revision = $household_storage->loadRevision($vid);
      // Only show revisions that are affected by the language that is being
      // displayed.
      if ($revision->hasTranslation($langcode) && $revision->getTranslation($langcode)->isRevisionTranslationAffected()) {
        $username = [
          '#theme' => 'username',
          '#account' => $revision->getRevisionUser(),
        ];

        // Use revision link to link to revisions that are not active.
        $date = \Drupal::service('date.formatter')->format($revision->getRevisionCreationTime(), 'short');
        if ($vid != $household->getRevisionId()) {
          $link = $this->l($date, new Url('entity.household.revision', ['household' => $household->id(), 'household_revision' => $vid]));
        }
        else {
          $link = $household->link($date);
        }

        $row = [];
        $column = [
          'data' => [
            '#type' => 'inline_template',
            '#template' => '{% trans %}{{ date }} by {{ username }}{% endtrans %}{% if message %}<p class="revision-log">{{ message }}</p>{% endif %}',
            '#context' => [
              'date' => $link,
              'username' => \Drupal::service('renderer')->renderPlain($username),
              'message' => ['#markup' => $revision->getRevisionLogMessage(), '#allowed_tags' => Xss::getHtmlTagList()],
            ],
          ],
        ];
        $row[] = $column;

        if ($latest_revision) {
          $row[] = [
            'data' => [
              '#prefix' => '<em>',
              '#markup' => $this->t('Current revision'),
              '#suffix' => '</em>',
            ],
          ];
          foreach ($row as &$current) {
            $current['class'] = ['revision-current'];
          }
          $latest_revision = FALSE;
        }
        else {
          $links = [];
          if ($revert_permission) {
            $links['revert'] = [
              'title' => $this->t('Revert'),
              'url' => $has_translations ?
              Url::fromRoute('entity.household.translation_revert', ['household' => $household->id(), 'household_revision' => $vid, 'langcode' => $langcode]) :
              Url::fromRoute('entity.household.revision_revert', ['household' => $household->id(), 'household_revision' => $vid]),
            ];
          }

          if ($delete_permission) {
            $links['delete'] = [
              'title' => $this->t('Delete'),
              'url' => Url::fromRoute('entity.household.revision_delete', ['household' => $household->id(), 'household_revision' => $vid]),
            ];
          }

          $row[] = [
            'data' => [
              '#type' => 'operations',
              '#links' => $links,
            ],
          ];
        }

        $rows[] = $row;
      }
    }

    $build['household_revisions_table'] = [
      '#theme' => 'table',
      '#rows' => $rows,
      '#header' => $header,
    ];

    return $build;
  }

}
