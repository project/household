<?php

namespace Drupal\household\Controller;

use Drupal\Component\Utility\Xss;
use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\DependencyInjection\ContainerInjectionInterface;
use Drupal\Core\Url;
use Drupal\household\Entity\IndividualInterface;

/**
 * Class IndividualController.
 *
 *  Returns responses for Individual routes.
 */
class IndividualController extends ControllerBase implements ContainerInjectionInterface {

  /**
   * Displays a Individual  revision.
   *
   * @param int $individual_revision
   *   The Individual  revision ID.
   *
   * @return array
   *   An array suitable for drupal_render().
   */
  public function revisionShow($individual_revision) {
    $individual = $this->entityManager()->getStorage('individual')->loadRevision($individual_revision);
    $view_builder = $this->entityManager()->getViewBuilder('individual');

    return $view_builder->view($individual);
  }

  /**
   * Page title callback for a Individual  revision.
   *
   * @param int $individual_revision
   *   The Individual  revision ID.
   *
   * @return string
   *   The page title.
   */
  public function revisionPageTitle($individual_revision) {
    $individual = $this->entityManager()->getStorage('individual')->loadRevision($individual_revision);
    return $this->t('Revision of %title from %date', ['%title' => $individual->label(), '%date' => format_date($individual->getRevisionCreationTime())]);
  }

  /**
   * Generates an overview table of older revisions of a Individual .
   *
   * @param \Drupal\household\Entity\IndividualInterface $individual
   *   A Individual  object.
   *
   * @return array
   *   An array as expected by drupal_render().
   */
  public function revisionOverview(IndividualInterface $individual) {
    $account = $this->currentUser();
    $langcode = $individual->language()->getId();
    $langname = $individual->language()->getName();
    $languages = $individual->getTranslationLanguages();
    $has_translations = (count($languages) > 1);
    $individual_storage = $this->entityManager()->getStorage('individual');

    $build['#title'] = $has_translations ? $this->t('@langname revisions for %title', ['@langname' => $langname, '%title' => $individual->label()]) : $this->t('Revisions for %title', ['%title' => $individual->label()]);
    $header = [$this->t('Revision'), $this->t('Operations')];

    $revert_permission = (($account->hasPermission("revert all individual revisions") || $account->hasPermission('administer individual entities')));
    $delete_permission = (($account->hasPermission("delete all individual revisions") || $account->hasPermission('administer individual entities')));

    $rows = [];

    $vids = $individual_storage->revisionIds($individual);

    $latest_revision = TRUE;

    foreach (array_reverse($vids) as $vid) {
      /** @var \Drupal\household\IndividualInterface $revision */
      $revision = $individual_storage->loadRevision($vid);
      // Only show revisions that are affected by the language that is being
      // displayed.
      if ($revision->hasTranslation($langcode) && $revision->getTranslation($langcode)->isRevisionTranslationAffected()) {
        $username = [
          '#theme' => 'username',
          '#account' => $revision->getRevisionUser(),
        ];

        // Use revision link to link to revisions that are not active.
        $date = \Drupal::service('date.formatter')->format($revision->getRevisionCreationTime(), 'short');
        if ($vid != $individual->getRevisionId()) {
          $link = $this->l($date, new Url('entity.individual.revision', ['individual' => $individual->id(), 'individual_revision' => $vid]));
        }
        else {
          $link = $individual->link($date);
        }

        $row = [];
        $column = [
          'data' => [
            '#type' => 'inline_template',
            '#template' => '{% trans %}{{ date }} by {{ username }}{% endtrans %}{% if message %}<p class="revision-log">{{ message }}</p>{% endif %}',
            '#context' => [
              'date' => $link,
              'username' => \Drupal::service('renderer')->renderPlain($username),
              'message' => ['#markup' => $revision->getRevisionLogMessage(), '#allowed_tags' => Xss::getHtmlTagList()],
            ],
          ],
        ];
        $row[] = $column;

        if ($latest_revision) {
          $row[] = [
            'data' => [
              '#prefix' => '<em>',
              '#markup' => $this->t('Current revision'),
              '#suffix' => '</em>',
            ],
          ];
          foreach ($row as &$current) {
            $current['class'] = ['revision-current'];
          }
          $latest_revision = FALSE;
        }
        else {
          $links = [];
          if ($revert_permission) {
            $links['revert'] = [
              'title' => $this->t('Revert'),
              'url' => $has_translations ?
              Url::fromRoute('entity.individual.translation_revert', ['individual' => $individual->id(), 'individual_revision' => $vid, 'langcode' => $langcode]) :
              Url::fromRoute('entity.individual.revision_revert', ['individual' => $individual->id(), 'individual_revision' => $vid]),
            ];
          }

          if ($delete_permission) {
            $links['delete'] = [
              'title' => $this->t('Delete'),
              'url' => Url::fromRoute('entity.individual.revision_delete', ['individual' => $individual->id(), 'individual_revision' => $vid]),
            ];
          }

          $row[] = [
            'data' => [
              '#type' => 'operations',
              '#links' => $links,
            ],
          ];
        }

        $rows[] = $row;
      }
    }

    $build['individual_revisions_table'] = [
      '#theme' => 'table',
      '#rows' => $rows,
      '#header' => $header,
    ];

    return $build;
  }

}
