<?php

namespace Drupal\household\Form;

use Drupal\Core\Entity\ContentEntityDeleteForm;

/**
 * Provides a form for deleting Household entities.
 *
 * @ingroup household
 */
class HouseholdDeleteForm extends ContentEntityDeleteForm {


}
