<?php

namespace Drupal\household\Form;

use Drupal\Core\Entity\EntityForm;
use Drupal\Core\Form\FormStateInterface;

/**
 * Class IndividualTypeForm.
 */
class IndividualTypeForm extends EntityForm {

  /**
   * {@inheritdoc}
   */
  public function form(array $form, FormStateInterface $form_state) {
    $form = parent::form($form, $form_state);

    $individual_type = $this->entity;
    $form['label'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Label'),
      '#maxlength' => 255,
      '#default_value' => $individual_type->label(),
      '#description' => $this->t("Label for the Individual type."),
      '#required' => TRUE,
    ];

    $form['id'] = [
      '#type' => 'machine_name',
      '#default_value' => $individual_type->id(),
      '#machine_name' => [
        'exists' => '\Drupal\household\Entity\IndividualType::load',
      ],
      '#disabled' => !$individual_type->isNew(),
    ];

    /* You will need additional form elements for your custom properties. */

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function save(array $form, FormStateInterface $form_state) {
    $individual_type = $this->entity;
    $status = $individual_type->save();

    switch ($status) {
      case SAVED_NEW:
        drupal_set_message($this->t('Created the %label Individual type.', [
          '%label' => $individual_type->label(),
        ]));
        break;

      default:
        drupal_set_message($this->t('Saved the %label Individual type.', [
          '%label' => $individual_type->label(),
        ]));
    }
    $form_state->setRedirectUrl($individual_type->toUrl('collection'));
  }

}
