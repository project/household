<?php

namespace Drupal\household\Form;

use Drupal\Core\Entity\ContentEntityDeleteForm;

/**
 * Provides a form for deleting Individual entities.
 *
 * @ingroup household
 */
class IndividualDeleteForm extends ContentEntityDeleteForm {


}
