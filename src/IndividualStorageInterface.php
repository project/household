<?php

namespace Drupal\household;

use Drupal\Core\Entity\ContentEntityStorageInterface;
use Drupal\Core\Session\AccountInterface;
use Drupal\Core\Language\LanguageInterface;
use Drupal\household\Entity\IndividualInterface;

/**
 * Defines the storage handler class for Individual entities.
 *
 * This extends the base storage class, adding required special handling for
 * Individual entities.
 *
 * @ingroup household
 */
interface IndividualStorageInterface extends ContentEntityStorageInterface {

  /**
   * Gets a list of Individual revision IDs for a specific Individual.
   *
   * @param \Drupal\household\Entity\IndividualInterface $entity
   *   The Individual entity.
   *
   * @return int[]
   *   Individual revision IDs (in ascending order).
   */
  public function revisionIds(IndividualInterface $entity);

  /**
   * Gets a list of revision IDs having a given user as Individual author.
   *
   * @param \Drupal\Core\Session\AccountInterface $account
   *   The user entity.
   *
   * @return int[]
   *   Individual revision IDs (in ascending order).
   */
  public function userRevisionIds(AccountInterface $account);

  /**
   * Counts the number of revisions in the default language.
   *
   * @param \Drupal\household\Entity\IndividualInterface $entity
   *   The Individual entity.
   *
   * @return int
   *   The number of revisions in the default language.
   */
  public function countDefaultLanguageRevisions(IndividualInterface $entity);

  /**
   * Unsets the language for all Individual with the given language.
   *
   * @param \Drupal\Core\Language\LanguageInterface $language
   *   The language object.
   */
  public function clearRevisionsLanguage(LanguageInterface $language);

}
