<?php

namespace Drupal\household;

use Drupal\Core\Entity\EntityAccessControlHandler;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Session\AccountInterface;
use Drupal\Core\Access\AccessResult;

/**
 * Access controller for the Individual entity.
 *
 * @see \Drupal\household\Entity\Individual.
 */
class IndividualAccessControlHandler extends EntityAccessControlHandler {

  /**
   * {@inheritdoc}
   */
  protected function checkAccess(EntityInterface $entity, $operation, AccountInterface $account) {
    /** @var \Drupal\household\Entity\IndividualInterface $entity */
    switch ($operation) {
      case 'view':
        if (!$entity->isPublished()) {
          return AccessResult::allowedIfHasPermission($account, 'view unpublished individual entities');
        }
        return AccessResult::allowedIfHasPermission($account, 'view published individual entities');

      case 'update':
        return AccessResult::allowedIfHasPermission($account, 'edit individual entities');

      case 'delete':
        return AccessResult::allowedIfHasPermission($account, 'delete individual entities');
    }

    // Unknown operation, no opinion.
    return AccessResult::neutral();
  }

  /**
   * {@inheritdoc}
   */
  protected function checkCreateAccess(AccountInterface $account, array $context, $entity_bundle = NULL) {
    return AccessResult::allowedIfHasPermission($account, 'add individual entities');
  }

}
