<?php

namespace Drupal\household\Entity;

use Drupal\Core\Entity\ContentEntityInterface;
use Drupal\Core\Entity\RevisionLogInterface;
use Drupal\Core\Entity\EntityChangedInterface;
use Drupal\user\EntityOwnerInterface;

/**
 * Provides an interface for defining Household entities.
 *
 * @ingroup household
 */
interface HouseholdInterface extends ContentEntityInterface, RevisionLogInterface, EntityChangedInterface, EntityOwnerInterface {

  // Add get/set methods for your configuration properties here.

  /**
   * Gets the Household name.
   *
   * @return string
   *   Name of the Household.
   */
  public function getName();

  /**
   * Sets the Household name.
   *
   * @param string $name
   *   The Household name.
   *
   * @return \Drupal\household\Entity\HouseholdInterface
   *   The called Household entity.
   */
  public function setName($name);

  /**
   * Gets the Household creation timestamp.
   *
   * @return int
   *   Creation timestamp of the Household.
   */
  public function getCreatedTime();

  /**
   * Sets the Household creation timestamp.
   *
   * @param int $timestamp
   *   The Household creation timestamp.
   *
   * @return \Drupal\household\Entity\HouseholdInterface
   *   The called Household entity.
   */
  public function setCreatedTime($timestamp);

  /**
   * Returns the Household published status indicator.
   *
   * Unpublished Household are only visible to restricted users.
   *
   * @return bool
   *   TRUE if the Household is published.
   */
  public function isPublished();

  /**
   * Sets the published status of a Household.
   *
   * @param bool $published
   *   TRUE to set this Household to published, FALSE to set it to unpublished.
   *
   * @return \Drupal\household\Entity\HouseholdInterface
   *   The called Household entity.
   */
  public function setPublished($published);

  /**
   * Gets the Household revision creation timestamp.
   *
   * @return int
   *   The UNIX timestamp of when this revision was created.
   */
  public function getRevisionCreationTime();

  /**
   * Sets the Household revision creation timestamp.
   *
   * @param int $timestamp
   *   The UNIX timestamp of when this revision was created.
   *
   * @return \Drupal\household\Entity\HouseholdInterface
   *   The called Household entity.
   */
  public function setRevisionCreationTime($timestamp);

  /**
   * Gets the Household revision author.
   *
   * @return \Drupal\user\UserInterface
   *   The user entity for the revision author.
   */
  public function getRevisionUser();

  /**
   * Sets the Household revision author.
   *
   * @param int $uid
   *   The user ID of the revision author.
   *
   * @return \Drupal\household\Entity\HouseholdInterface
   *   The called Household entity.
   */
  public function setRevisionUserId($uid);

}
