<?php

namespace Drupal\household\Entity;

use Drupal\Core\Config\Entity\ConfigEntityBundleBase;

/**
 * Defines the Individual type entity.
 *
 * @ConfigEntityType(
 *   id = "individual_type",
 *   label = @Translation("Individual type"),
 *   handlers = {
 *     "view_builder" = "Drupal\Core\Entity\EntityViewBuilder",
 *     "list_builder" = "Drupal\household\IndividualTypeListBuilder",
 *     "form" = {
 *       "add" = "Drupal\household\Form\IndividualTypeForm",
 *       "edit" = "Drupal\household\Form\IndividualTypeForm",
 *       "delete" = "Drupal\household\Form\IndividualTypeDeleteForm"
 *     },
 *     "route_provider" = {
 *       "html" = "Drupal\household\IndividualTypeHtmlRouteProvider",
 *     },
 *   },
 *   config_prefix = "individual_type",
 *   admin_permission = "administer site configuration",
 *   bundle_of = "individual",
 *   entity_keys = {
 *     "id" = "id",
 *     "label" = "label",
 *     "uuid" = "uuid"
 *   },
 *   links = {
 *     "canonical" = "/admin/structure/individual_type/{individual_type}",
 *     "add-form" = "/admin/structure/individual_type/add",
 *     "edit-form" = "/admin/structure/individual_type/{individual_type}/edit",
 *     "delete-form" = "/admin/structure/individual_type/{individual_type}/delete",
 *     "collection" = "/admin/structure/individual_type"
 *   }
 * )
 */
class IndividualType extends ConfigEntityBundleBase implements IndividualTypeInterface {

  /**
   * The Individual type ID.
   *
   * @var string
   */
  protected $id;

  /**
   * The Individual type label.
   *
   * @var string
   */
  protected $label;

}
