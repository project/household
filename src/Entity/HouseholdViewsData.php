<?php

namespace Drupal\household\Entity;

use Drupal\views\EntityViewsData;

/**
 * Provides Views data for Household entities.
 */
class HouseholdViewsData extends EntityViewsData {

  /**
   * {@inheritdoc}
   */
  public function getViewsData() {
    $data = parent::getViewsData();

    // Additional information for Views integration, such as table joins, can be
    // put here.

    return $data;
  }

}
