<?php

namespace Drupal\household\Entity;

use Drupal\Core\Entity\ContentEntityInterface;
use Drupal\Core\Entity\RevisionLogInterface;
use Drupal\Core\Entity\EntityChangedInterface;
use Drupal\user\EntityOwnerInterface;

/**
 * Provides an interface for defining Individual entities.
 *
 * @ingroup household
 */
interface IndividualInterface extends ContentEntityInterface, RevisionLogInterface, EntityChangedInterface, EntityOwnerInterface {

  // Add get/set methods for your configuration properties here.

  /**
   * Gets the Individual name.
   *
   * @return string
   *   Name of the Individual.
   */
  public function getName();

  /**
   * Sets the Individual name.
   *
   * @param string $name
   *   The Individual name.
   *
   * @return \Drupal\household\Entity\IndividualInterface
   *   The called Individual entity.
   */
  public function setName($name);

  /**
   * Gets the Individual creation timestamp.
   *
   * @return int
   *   Creation timestamp of the Individual.
   */
  public function getCreatedTime();

  /**
   * Sets the Individual creation timestamp.
   *
   * @param int $timestamp
   *   The Individual creation timestamp.
   *
   * @return \Drupal\household\Entity\IndividualInterface
   *   The called Individual entity.
   */
  public function setCreatedTime($timestamp);

  /**
   * Returns the Individual published status indicator.
   *
   * Unpublished Individual are only visible to restricted users.
   *
   * @return bool
   *   TRUE if the Individual is published.
   */
  public function isPublished();

  /**
   * Sets the published status of a Individual.
   *
   * @param bool $published
   *   TRUE to set this Individual to published, FALSE to set it to unpublished.
   *
   * @return \Drupal\household\Entity\IndividualInterface
   *   The called Individual entity.
   */
  public function setPublished($published);

  /**
   * Gets the Individual revision creation timestamp.
   *
   * @return int
   *   The UNIX timestamp of when this revision was created.
   */
  public function getRevisionCreationTime();

  /**
   * Sets the Individual revision creation timestamp.
   *
   * @param int $timestamp
   *   The UNIX timestamp of when this revision was created.
   *
   * @return \Drupal\household\Entity\IndividualInterface
   *   The called Individual entity.
   */
  public function setRevisionCreationTime($timestamp);

  /**
   * Gets the Individual revision author.
   *
   * @return \Drupal\user\UserInterface
   *   The user entity for the revision author.
   */
  public function getRevisionUser();

  /**
   * Sets the Individual revision author.
   *
   * @param int $uid
   *   The user ID of the revision author.
   *
   * @return \Drupal\household\Entity\IndividualInterface
   *   The called Individual entity.
   */
  public function setRevisionUserId($uid);

}
