<?php

namespace Drupal\household\Entity;

use Drupal\Core\Config\Entity\ConfigEntityInterface;

/**
 * Provides an interface for defining Individual type entities.
 */
interface IndividualTypeInterface extends ConfigEntityInterface {

  // Add get/set methods for your configuration properties here.
}
