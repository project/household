<?php

namespace Drupal\household;

use Drupal\Core\Entity\Sql\SqlContentEntityStorage;
use Drupal\Core\Session\AccountInterface;
use Drupal\Core\Language\LanguageInterface;
use Drupal\household\Entity\HouseholdInterface;

/**
 * Defines the storage handler class for Household entities.
 *
 * This extends the base storage class, adding required special handling for
 * Household entities.
 *
 * @ingroup household
 */
class HouseholdStorage extends SqlContentEntityStorage implements HouseholdStorageInterface {

  /**
   * {@inheritdoc}
   */
  public function revisionIds(HouseholdInterface $entity) {
    return $this->database->query(
      'SELECT vid FROM {household_revision} WHERE id=:id ORDER BY vid',
      [':id' => $entity->id()]
    )->fetchCol();
  }

  /**
   * {@inheritdoc}
   */
  public function userRevisionIds(AccountInterface $account) {
    return $this->database->query(
      'SELECT vid FROM {household_field_revision} WHERE uid = :uid ORDER BY vid',
      [':uid' => $account->id()]
    )->fetchCol();
  }

  /**
   * {@inheritdoc}
   */
  public function countDefaultLanguageRevisions(HouseholdInterface $entity) {
    return $this->database->query('SELECT COUNT(*) FROM {household_field_revision} WHERE id = :id AND default_langcode = 1', [':id' => $entity->id()])
      ->fetchField();
  }

  /**
   * {@inheritdoc}
   */
  public function clearRevisionsLanguage(LanguageInterface $language) {
    return $this->database->update('household_revision')
      ->fields(['langcode' => LanguageInterface::LANGCODE_NOT_SPECIFIED])
      ->condition('langcode', $language->getId())
      ->execute();
  }

}
