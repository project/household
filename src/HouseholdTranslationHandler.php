<?php

namespace Drupal\household;

use Drupal\content_translation\ContentTranslationHandler;

/**
 * Defines the translation handler for household.
 */
class HouseholdTranslationHandler extends ContentTranslationHandler {

  // Override here the needed methods from ContentTranslationHandler.

}
