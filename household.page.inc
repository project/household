<?php

/**
 * @file
 * Contains household.page.inc.
 *
 * Page callback for Household entities.
 */

use Drupal\Core\Render\Element;

/**
 * Prepares variables for Household templates.
 *
 * Default template: household.html.twig.
 *
 * @param array $variables
 *   An associative array containing:
 *   - elements: An associative array containing the user information and any
 *   - attributes: HTML attributes for the containing element.
 */
function template_preprocess_household(array &$variables) {
  // Fetch Household Entity Object.
  $household = $variables['elements']['#household'];

  // Helpful $content variable for templates.
  foreach (Element::children($variables['elements']) as $key) {
    $variables['content'][$key] = $variables['elements'][$key];
  }
}
