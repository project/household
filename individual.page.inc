<?php

/**
 * @file
 * Contains individual.page.inc.
 *
 * Page callback for Individual entities.
 */

use Drupal\Core\Render\Element;

/**
 * Prepares variables for Individual templates.
 *
 * Default template: individual.html.twig.
 *
 * @param array $variables
 *   An associative array containing:
 *   - elements: An associative array containing the user information and any
 *   - attributes: HTML attributes for the containing element.
 */
function template_preprocess_individual(array &$variables) {
  // Fetch Individual Entity Object.
  $individual = $variables['elements']['#individual'];

  // Helpful $content variable for templates.
  foreach (Element::children($variables['elements']) as $key) {
    $variables['content'][$key] = $variables['elements'][$key];
  }
}
